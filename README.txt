Cache Status
============

This module provides a graph where you can see the size of the cache tables and
labels marking some of the events that may empty the cache.

You can see how much damage those events do to your cache size.

The events recorded are:
 * Cron run
 * Insert, update, delete for:
   * Nodes
   * Users
   * Taxonomy terms and vocabularies


Installation
============

1. Enable the module and its dependencies (chart).

2. Configure a cron job to execute the script cache_status_cron.php

   The amount of time can be anything from 1 minute.

   Example cron job:
* * * * * /usr/local/bin/wget -q -O /dev/null http://localhost/sites/all/modules/cache_status/cache_status_cron.php

3. See the charts at admin/reports/cache/graph

4. Configure the module at admin/reports/cache/settings

5. See an overview of the current cache status at admin/reports/cache
