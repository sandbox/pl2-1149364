<?php

/**
 * Cache status reports page.
 */
function cache_status_reports_page() {
  $tables = _cache_status_cache_table_names();

  $headers = array(t('Table'), t('Entry count'));
  $rows = array();
  foreach ($tables as $table_name) {
    $count = db_result(db_query("SELECT COUNT(*) FROM {". $table_name ."}"));
    $rows[] = array(check_plain($table_name), $count);
  }

  $output = '';

  $output .= '<h2>'. t('Cache tables') .'</h2>';
  $output .= theme('table', $headers, $rows);

  $output .= drupal_get_form('cache_status_record_status_form');

  $output .= '<h2>'. t('Recent events') .'</h2>';
  $result = db_query("SELECT * FROM {cache_status_events} ORDER BY timestamp DESC LIMIT %d", 20);
  $headers = array(t('Date'), t('Type'));
  $rows = array();
  while ($row = db_fetch_object($result)) {
    $rows[] = array(format_date($row->timestamp), $row->type);
  }

  $output .= theme('table', $headers, $rows);

  return $output;
}

/**
 * Cache status reports record form.
 */
function cache_status_record_status_form($form_state) {
  $form = array();

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Records status'),
  );

  return $form;
}

/**
 * Cache status report record form submit handler.
 */
function cache_status_record_status_form_submit(&$form_state) {
  cache_status_record_status();

  drupal_set_message(t('Cache status recorded'));
}

function cache_status_reports_graph() {
  $tables = _cache_status_cache_table_names();
  $selected = variable_get('cache_status_graph_tables', array());
  $tables = array_intersect($tables, $selected);

  $output = '';

  foreach ($tables as $table_name) {
    $output .= '<h2>'. $table_name .'</h2>';
    $output .= cache_status_reports_graph_table($table_name);
  }

  return $output;
}

function cache_status_reports_graph_table($table_name) {
  $chart = array(
    '#chart_id' => 'cache_status_'. $table_name,
    '#type' => CHART_TYPE_LINE,
    '#size' => chart_size(700, 350),
    '#grid_lines' => chart_grid_lines(16.66, 10, 1, 3),
    '#adjust_resolution' => FALSE,
  );

  $limit = variable_get('cache_status_graph_data_points', 300);

  $max = 0;
  $result = db_query("SELECT * FROM (SELECT * FROM {cache_status_sizes} WHERE cache_table = '%s' ORDER BY timestamp DESC LIMIT %d) s ORDER BY timestamp ASC", $table_name, $limit);
  $min_ts = db_result(db_query("SELECT MIN(timestamp) FROM (SELECT * FROM {cache_status_sizes} WHERE cache_table = '%s' ORDER BY timestamp DESC LIMIT %d) s ORDER BY timestamp ASC", $table_name, $limit));

  $i = 0;
  $prev_ts = $min_ts;
  while ($row = db_fetch_object($result)) {
    $chart['#data']['size'][$row->timestamp] = $row->entry_count;
    $max = $row->entry_count > $max ? $row->entry_count : $max;
    $ev_result = db_query("SELECT * FROM {cache_status_events} WHERE timestamp > %d AND timestamp <= %d", $prev_ts, $row->timestamp);
    while ($ev_row = db_fetch_object($ev_result)) {
      $chart['#shape_markers'][] = array('A'. $ev_row->type, '', 0, $i - 1, 9);
    }
    $prev_ts = $row->timestamp;
    $i++;
  }

  // Scale the data, it must be in %
  $top = $max * 2;
  foreach ($chart['#data']['size'] as $delta => $size) {
    $chart['#data']['size'][$delta] = (int)round(($size * 100) / $top);
  }


  $chart['#mixed_axis_labels'][CHART_AXIS_Y_LEFT][0][] = chart_mixed_axis_range_label(0, $top);

  $output = chart_render($chart);
  return $output;
}

function cache_status_reports_settings_form($form_state) {
  $form = array();

  $form['cache_status_graph_data_points'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of data points to graph'),
    '#default_value' => variable_get('cache_status_graph_data_points', 300),
    '#description' => t('Setting this value too high can break the graphs. Lower it if the graphs are not shown.'),
  );

  $form['cache_status_store_data_points'] = array(
    '#type' => 'textfield',
    '#title' => t('Max data points to store'),
    '#default_value' => variable_get('cache_status_store_data_points', 300),
    '#description' => t('The number of data points that will be kept in the database. Older data will be deleted. This avoids having a huge table in the database.'),
  );

  $tables = _cache_status_cache_table_names();
  $options = array_combine($tables, $tables);

  $form['cache_status_graph_tables'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Graph tables'),
    '#options' => $options,
    '#default_value' => variable_get('cache_status_graph_tables', array()),
  );

  return system_settings_form($form);
}
