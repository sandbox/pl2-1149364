<?php

/**
 * This script will try to detect the path Drupal is installed in.
 * In case the script crashes when executed, you must especify the base
 * path of your Drupal install here.
 */
$path = '';

if (empty($path)) {
  if (!file_exists('includes/bootstrap.inc') && preg_match('@^(.*)[\\\\/]sites[\\\\/][^\\\\/]+[\\\\/]modules[\\\\/][^\\\\/]+[\\\\/]cache_status$@', getcwd(), $r)) {
    $path = $r[1];
  }
}

chdir($path);

include_once './includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

if (function_exists('cache_status_record_status')) {
  cache_status_record_status();
}
